@extends('index')

@section('css_js')
<!-- Required meta tags -->
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>STRICT</title>
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="{{ asset('homepage')}}/css/all.min.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('homepage')}}/css/bootstrap.min.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('homepage')}}/css/style.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{ asset('homepage')}}/css/responsive.css">
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('homepage')}}/js/jquery-3.3.1.slim.min.js"></script>
    <script src="{{ asset('homepage')}}/js/popper.min.js"></script>
    <script src="{{ asset('homepage')}}/js/bootstrap.min.js"></script>
@endsection