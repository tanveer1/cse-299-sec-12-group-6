<header class="header_area">
        <div class="main_menu">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="#"><img src="{{ asset('homepage')}}/images/logo.png" alt="logo" class="img-fluid"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="#"> HOME <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"> LOG IN </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"> EVENTS </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"> SEARCH </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"> CONTACT US</a>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </header>