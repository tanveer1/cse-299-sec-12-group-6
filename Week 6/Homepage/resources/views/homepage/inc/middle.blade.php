<div class="image_area">
        <div class="overlay text-white">

            <div class="content_area text-center text-white">
                <h1>STRICT EVENT MANAGER</h1>

                <div class="line"></div>

                <h5>STRICT Event Manager is a website where event organizers can create events.</h5>

                <div class="call">
                    <a href="#">SIGN UP</a>
                </div>

                <div class="icon">
                    <a href="#"><i class="fas fa-angle-down"></i></a>
                </div>
            </div>
        </div>
    </div>