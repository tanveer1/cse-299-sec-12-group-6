<!DOCTYPE html>
<html lang="en">

<head>
    @yield('css_js')
</head>

<body>
    <!--   header part start -->
        @include('homepage.inc.menu')
    <!--   header part end -->

    <!-- Image part start -->
        @include('homepage.inc.middle')
    <!-- Image part end -->

    <!-- Footer part start -->
        @include('homepage.inc.footer')
    <!-- Footer part end -->




    
</body>

</html>